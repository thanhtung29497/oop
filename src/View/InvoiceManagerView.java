/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.*;
import java.awt.Point;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import utils.*;

/**
 *
 * @author macos
 */
public class InvoiceManagerView extends javax.swing.JPanel implements IObserver<ArrayList<Invoice>> {

    Model model;
    ArrayList<Invoice> invoices;
    DefaultTableModel invoiceTableModel;
    DefaultTableModel invoiceDetailTableModel;
    Integer currentSelectedRow = -1;
    
    /**
     * Creates new form CostManager
     * @param model
     */
    public InvoiceManagerView(Model model) {
        this.model = model;
        initComponents();
        this.invoiceTableModel = (DefaultTableModel)this.invoiceTable.getModel();
        this.invoiceDetailTableModel = (DefaultTableModel)this.invoiceDetailTable.getModel();
        this.invoices = new ArrayList<>();
        this.model.addInvoiceObserver(this);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        invoiceTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        invoiceDetailTable = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        typeLabel = new javax.swing.JLabel();
        typeOptions = new javax.swing.JComboBox<>();
        chooseButton = new javax.swing.JButton();
        idLabel = new javax.swing.JLabel();
        findButton = new javax.swing.JButton();
        idTextField = new javax.swing.JTextField();

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel1.setText("Quản lý hoá đơn");

        invoiceTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "ID", "Date", "Total", "Type", "Note"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        invoiceTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                invoiceTableMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(invoiceTable);

        jLabel2.setText("Danh sách hoá đơn");

        invoiceDetailTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Name", "Cost", "Quantity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(invoiceDetailTable);

        jLabel3.setText("Chi tiết hoá đơn");

        typeLabel.setText("Type:");

        typeOptions.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Import Invoice", "Sale Invoice", "All" }));

        chooseButton.setText("Choose");
        chooseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseButtonActionPerformed(evt);
            }
        });

        idLabel.setText("ID");

        findButton.setText("Find");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(419, 419, 419))
            .addGroup(layout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(200, 200, 200))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 473, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(typeLabel)
                        .addGap(18, 18, 18)
                        .addComponent(typeOptions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(chooseButton)
                        .addGap(144, 144, 144)
                        .addComponent(idLabel)
                        .addGap(18, 18, 18)
                        .addComponent(idTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(findButton)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(typeLabel)
                    .addComponent(typeOptions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chooseButton)
                    .addComponent(idLabel)
                    .addComponent(findButton)
                    .addComponent(idTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(63, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void chooseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chooseButtonActionPerformed

    private void invoiceTableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_invoiceTableMousePressed
        // TODO add your handling code here:
        Point point = evt.getPoint();
        Integer row = this.invoiceTable.rowAtPoint(point);
        if (row != -1 && !this.invoices.isEmpty()) {
            Invoice invoice = this.invoices.get(row);
            this.refreshInvoiceDetailTable(invoice);
        }
    }//GEN-LAST:event_invoiceTableMousePressed

    private void refreshInvoiceTable() {
        this.invoiceTableModel.getDataVector().removeAllElements();
        for (Invoice invoice: this.invoices) {
            Object[] row = RowConverter.toInvoiceRow(invoice);
            this.invoiceTableModel.addRow(row);
        }
//        this.refreshInvoiceDetailTable(invoices.get(0));
        this.updateUI();
    }
    
    private void refreshInvoiceDetailTable(Invoice invoice) {
        this.invoiceDetailTableModel.getDataVector().removeAllElements();
        if (invoice instanceof SaleInvoice) {
            for (SaleItem saleItem: ((SaleInvoice) invoice).getSaleItems()) {
                Object[] row = RowConverter.toSaleRow(saleItem);
                this.invoiceDetailTableModel.addRow(row);
            }
        } else if (invoice instanceof ImportInvoice) {
            for (ImportItem importItem: ((ImportInvoice) invoice).getImportItems()) {
                Object[] row = RowConverter.toImportRow(importItem);
                this.invoiceDetailTableModel.addRow(row);
            }
        }
    }
    
    public void update(ArrayList<Invoice> invoices) {
        this.invoices = invoices;
        this.refreshInvoiceTable();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton chooseButton;
    private javax.swing.JButton findButton;
    private javax.swing.JLabel idLabel;
    private javax.swing.JTextField idTextField;
    private javax.swing.JTable invoiceDetailTable;
    private javax.swing.JTable invoiceTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel typeLabel;
    private javax.swing.JComboBox<String> typeOptions;
    // End of variables declaration//GEN-END:variables
}
