/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author thanh
 */

public class SpinnerCellEditor extends AbstractCellEditor implements TableCellEditor {
    private final JSpinner spinner = new JSpinner();

    public SpinnerCellEditor(SpinnerNumberModel spinnerModel) {
        spinner.setModel(spinnerModel);
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
        int row, int column) {
        spinner.setValue(value);
        return spinner;
    }

    public boolean isCellEditable() {
      return true;
    }

    @Override
    public Object getCellEditorValue() {
        return spinner.getValue();
    }
}
