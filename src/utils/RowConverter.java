/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.*;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author thanh
 */
public class RowConverter {
    public static Object[] toProductRow(Item item) {
        Object[] row = new Object[4];
        row[0] = item.getId();
        row[1] = item.getName();
        row[2] = item.getCost();
        row[3] = item.getQuantity();
        return row;
    }
    
    public static Object[] toSaleRow(SaleItem saleItem) {
        Item item = saleItem.getItem();
        Object[] row = new Object[5];
        row[0] = item.getId();
        row[1] = item.getName();
        row[2] = saleItem.getCost();
        row[3] = saleItem.getQuantity();
        row[4] = saleItem.getCost() * saleItem.getQuantity();
        return row;
    }
    
    public static Object[] toInvoiceRow(Invoice invoice) {
        Object[] row = new Object[5];
        row[0] = invoice.getId();
        row[1] = RowConverter.formatDate(invoice.getCreatedTime());
        row[2] = Math.abs(invoice.getSumCost());
        row[3] = invoice.getType();
        row[4] = invoice.getDescription();
        return row;
    }
    
    public static Object[] toStaffRow(Staff staff) {
        Object[] row = new Object[7];
        row[0] = staff.getId();
        row[1] = staff.getName();
        row[2] = staff.getDateOfBirth() != null ? RowConverter.formatDate(staff.getDateOfBirth()) : "";
        row[3] = staff.getPosition().getName();
        row[4] = staff.getSalary();
        row[5] = 0.0;
        row[6] = 0.0;
        return row;
    }
    
    public static Object[] toImportRow(ImportItem importItem) {
        Item item = importItem.getItem();
        Object[] row = new Object[5];
        row[0] = item.getId();
        row[1] = item.getName();
        row[2] = importItem.getPurchase();
        row[3] = importItem.getQuantity();
        row[4] = importItem.getPurchase() * importItem.getQuantity();
        return row;
    }
    
    public static String formatDate(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }
}
