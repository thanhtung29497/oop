package utils;

/**
 * Created by thanh on 10/3/2017.
 */
public class IdGenerator {
    private static Integer currentId;
    public static String generateId() {
        if (IdGenerator.currentId == null) {
            IdGenerator.currentId = 0;
        }
        IdGenerator.currentId += 1;
        return  IdGenerator.currentId.toString();
    }
}
