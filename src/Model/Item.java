package Model;

import java.util.Date;

public class Item extends Entity {
    private String name;
    private Double cost;
    private Double purchase;
    private Integer quantity;
    private String basicUnit;
    private Date expirationDate;

    public Item(String name, Double cost, Double purchase, String basicUnit, Date expirationDate){}

    public Item(String name, Double cost, Double purchase) {
        super();
    }

    public Item() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getCost() {
        return cost;
    }

    public void setPurchase(Double purchase) {
        this.purchase = purchase;
    }

    public Double getPurchase() {
        return purchase;
    }

    public String getBasicUnit() {
        return basicUnit;
    }

    public void setBasicUnit(String basicUnit) {
        this.basicUnit = basicUnit;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}