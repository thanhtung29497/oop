/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author thanh
 */
public class SalaryPayment extends Entity {
    private Staff staff;
    private Double payPerHour;
    private Integer totalHours;
    
    public void setPayPerHour(Double payPerHour) {
        this.payPerHour = payPerHour;
    }
    
    public Double getPayPerHour() {
        return this.payPerHour;
    }
    
    public void setTotalHours(Integer totalHours) {
        this.totalHours = totalHours;
    }
    
    public Integer getTotalHours() {
        return this.totalHours;
    }
    
    public void setStaff(Staff staff) {
        this.staff = staff;
    }
    
    public Staff getStaff() {
        return this.staff;
    }
    
    public Double getAmount() {
        return this.totalHours * this.payPerHour;
    }
    
}
