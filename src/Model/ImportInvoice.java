package Model;

import java.util.ArrayList;
import java.util.Date;

public class ImportInvoice extends Invoice {
    private ArrayList<ImportItem> importItems;
    public ImportInvoice () {
        this.type = InvoiceType.Import;
        this.importItems = new ArrayList<ImportItem>();
        this.sumCost = 0.0;
    }

    public ArrayList<ImportItem> getImportItems() {
        return importItems;
    }

    public void addItems(ArrayList<ImportItem> items){
        for (ImportItem item: items) {
            this.importItems.add(item);
            this.sumCost -= item.getQuantity() * item.getPurchase();
        }
    }
    
    @Override
    public Double getSumCost() {
        return this.sumCost;
    }

    public void addItem(ImportItem item) {
        this.sumCost -= item.getPurchase();
        this.importItems.add(item);
    }
}
