package Model;

import java.util.ArrayList;
import Data.*;
import exception.NotExistStaffException;

public class Human {
    private ArrayList<Staff> staffs = Data.staffs;


    public boolean fire(Staff staff) {
        staffs.remove(staff);
        return false;
    }

    public void hire(Staff staff) {
        staffs.add(staff);
        System.out.print(staffs.size());
    }

    public boolean pay(String id) {

        return false;
    }

    public void payAll() {
        for (Staff staff: staffs) {
            pay(staff.getId());
        }
    }

    public ArrayList<Staff> getAllStaffs() {


        return staffs;
    }

    public void setStaffs(ArrayList<Staff> staffs) {
        this.staffs = staffs;
    }
    
    public Staff getStaffById(String id) throws NotExistStaffException {
        for (Staff staff: this.staffs) {
            if (staff.getId().equals(id)) {
                return staff;
            }
        }
        throw new NotExistStaffException();
    }
}
