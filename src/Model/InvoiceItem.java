/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author thanh
 */
public class InvoiceItem extends Entity{
    protected Item item;
    protected Integer quantity;
    
    public InvoiceItem(Item item, Integer quantity) {
        this.item = item;
        this.quantity = quantity;
    }
    
    public Item getItem() {
        return item;
    }
    
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    
    public Integer getQuantity() {
        return this.quantity;
    }
}
