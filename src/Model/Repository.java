package Model;


import java.util.ArrayList;
//import java.util.HashMap;
//import Data.*;
//import static View.ProductView.products;
import exception.*;
import query.*;

public class Repository {
    private ArrayList<Item> items; 
    
    public Repository() {
        this.items = new ArrayList<>();
    }
    
    public Item find(String id) throws NotExistItemException {
        for (Item item: items) {
            if (item.getId().equals(id)) {
                return item;
            }
        }
        throw new NotExistItemException();
    }
    
    public boolean checkIfEnoughItem(String id, Integer quantity) {
        try {
            Item item = this.find(id);
            return item.getQuantity() >= quantity;
        } catch (NotExistItemException e) {
            return false;
        }
    }

    public void importItems(ImportInvoice importInvoice) {
        for (ImportItem importItem: importInvoice.getImportItems()) {
            try {
                Item item = this.find(importItem.getItem().getId());
                item.setQuantity(item.getQuantity() + importItem.getQuantity());
            } catch (NotExistItemException e) {
                this.addItem(importItem.getItem());
            }
        }
        
//        for (Item item: importItems){
//            Boolean isFound = false;
//            for (int i = 0; i < Data.products.size(); i ++){
//                Item product = Data.products.get(i);
//                if (product.getName().equals(item.getName()) && product.getCost() == item.getCost()){
//                    isFound = true;
//                    product.setQuantity(product.getQuantity() + item.getQuantity());
//                    break;
//                }
//            }
//            if (!isFound){
//                Data.products.add(item);
//            }
//        }
//        
//        Data.importInvoices.add(importInvoice);
    }
    
    public void addItem(Item item) {
        this.items.add(item);
    }

    public void remove(ArrayList<Item> itemsToRemove) {
        
    }

    public void exportItems(SaleInvoice saleInvoice) {
        for (SaleItem saleItem: saleInvoice.getSaleItems()) {
            try {
                Item item = this.find(saleItem.getItem().getId());
                item.setQuantity(item.getQuantity() - saleItem.getQuantity());
            } catch (NotExistItemException e) {
                e.printStackTrace();
            }
        }
//        saleInvoice.setSaleItems(saleItems);
//        for (Item item: saleInvoice.getSaleItems()){
//            for (int i = 0; i < Data.products.size(); i ++){
//                Item product = Data.products.get(i);
//                if (product.getId().equals(item.getId())){
//                    product.setQuantity(product.getQuantity() - item.getQuantity());
//                    break;
//                }
//            }
//        }
//        
//        Data.saleInvoices.add(saleInvoice);
//        // return invoice with valid item
//        return saleInvoice;
    }



    public ArrayList<Item> getAllItems () {
        return this.items;
    }
    
    public ArrayList<Item> getItemsByQuery(ArrayList<Query> queries) {
        ArrayList<Item> result = new ArrayList<>();
        this.items.stream().filter((item) -> {
            boolean satisfied = true;
            for (Query query: queries) {
                if (!query.isMatched(item)) {
                    satisfied = false;
                    break;
                }
            }
            return satisfied;
        }).forEachOrdered((item) -> {
            result.add(item);
        });
        return result;
    }
}
