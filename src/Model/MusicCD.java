package Model;

/**
 * Created by thanh on 10/3/2017.
 */
public class MusicCD extends Item {
    private String singer;
    private String writer;
    private String type;

    public MusicCD(String name, String type, String singer, String writer, Double cost, Double purchase) {
        super(name, cost, purchase);
    }

    public MusicCD() {

    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getSinger() {
        return singer;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
