package Model;

import java.time.Month;

// save paid salary history for each staff
public class SalaryRecord extends Entity {
    private Month month;
    private Double salaryPaid;

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public Double getSalaryPaid() {
        return salaryPaid;
    }

    public void setSalaryPaid(Double salaryPaid) {
        this.salaryPaid = salaryPaid;
    }
}
