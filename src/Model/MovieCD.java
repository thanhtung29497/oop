package Model;

import java.util.Date;

/**
 * Created by thanh on 10/3/2017.
 */
public class MovieCD extends Item {
    private String cast;
    private String type;
    private String director;
    private Date releaseDate;

    public MovieCD(String name, String type, String cast[], String director, Double cost, Double purchase) {
        super(name, cost, purchase);
    }

    public MovieCD() {

    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
}
