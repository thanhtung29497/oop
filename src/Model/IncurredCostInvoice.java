package Model;

import java.util.Date;

public class IncurredCostInvoice extends Invoice {

    public IncurredCostInvoice(String description, Double cost, Date time, InvoiceType type) {
        super(type, time, description, cost);
    }

    public IncurredCostInvoice() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
//    public IncurredCost(String description, Double cost, String time) {
//        super();
//        this.description = description;
//        this.cost = cost;
//        this.time = time;
//    }
    
    
    
    @Override
    public Double getSumCost() {
        return 0.0;
    }
}
