package Model;

public class StaffPosition {
    private String positionName;
    public StaffPosition(String positionName) {
        this.positionName = positionName;
    }
    
    public String getName() {
        return this.positionName;
    }
    
    public static StaffPosition Manager = new StaffPosition("Manager");
    public static StaffPosition Cashier = new StaffPosition("Cashier");
    public static StaffPosition Storekeeper = new StaffPosition("Storekeeper");
    public static StaffPosition Accountant = new StaffPosition("Accountant");
}
