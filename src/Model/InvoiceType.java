package Model;

public enum InvoiceType {
    Sale,
    Import,
    Salary, 
    IncurredCost
}
