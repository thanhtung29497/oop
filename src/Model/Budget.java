package Model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import Data.*;

public class Budget {
    private static Double currentBudget = 100000000.0;
    private ArrayList<Invoice> savedInvoices = new ArrayList<Invoice>();
    private ArrayList<IncurredCostInvoice> incurredCosts = Data.incurredCosts;
    private ArrayList<SaleInvoice> saleInvoices = Data.saleInvoices;

    public void processInvoice(Invoice invoice) {
        this.currentBudget += invoice.getSumCost();
        this.savedInvoices.add(invoice);
        
//        if (invoice instanceof ImportInvoice) {
//            
//        } else if (invoice instanceof SaleInvoice) {
//            //todo
//        }
        // save invoice to db

    }
    
    public void removeInvoice(Invoice invoice){
        this.currentBudget -= invoice.getSumCost();
        this.savedInvoices.remove(invoice);
    }
    
    public void removeCost(IncurredCostInvoice cost){
        Data.incurredCosts.remove(cost);
    }

    public Double getIncome(Date startTime, Date endTime) {
        Double income = 0.0;
        for (Invoice invoice : this.getInvoiceList()) {
            Date createTime = invoice.getCreatedTime();
            if (createTime.before(endTime) && createTime.after(startTime) && invoice instanceof SaleInvoice) {
                income += ((SaleInvoice) invoice).getSumCost();
            }
        }
        return income;
    }

    public Double getProfit(Date startTime, Date endTime) {
        Double profit = 0.0;
        for (Invoice invoice : this.getInvoiceList()) {
            Date createTime = invoice.getCreatedTime();
            if (createTime.before(endTime) && createTime.after(startTime)) {
                profit += invoice.getSumCost();
            }
        }
        return profit;
    }

    public static Double getCurrentBudget() {
        return currentBudget;
    }
    public static void setCurrentBudget(Double currentBudget1){
        currentBudget = currentBudget1;
    }

    public ArrayList<Invoice> getInvoiceList() {
        return this.savedInvoices;
    }
    
    public void addNewCost(IncurredCostInvoice incurredCost){
        incurredCosts.add(incurredCost);
    }
    
    public ArrayList<SaleInvoice> getSaleInvoices(){
        return Data.saleInvoices;
    }

    public ArrayList<ImportInvoice> getImportInvoices(){
        return Data.importInvoices;
    }
    
    public ArrayList<IncurredCostInvoice> getIncurredCosts(){
        return Data.incurredCosts;
    }
    
}
