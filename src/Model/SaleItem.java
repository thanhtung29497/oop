package Model;

import java.util.Date;

public class SaleItem extends InvoiceItem{

    private Double cost;

    public SaleItem(Item item, Integer quantity, Double cost) {
        super(item, quantity);
        this.cost = cost;
    }
    
    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getCost() {
        return cost;
    }

}
