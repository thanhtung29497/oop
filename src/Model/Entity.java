package Model;

import utils.IdGenerator;

public class Entity {
    private String id;

    public Entity() {
        this.id = IdGenerator.generateId();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}