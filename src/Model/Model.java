package Model;
import Data.*;
import java.util.ArrayList;
import exception.*;
import utils.IObserver;
import query.*;

public class Model {
    private final Budget budget;
    private final Repository repository;
    private final Human human;
    private final ArrayList<IObserver<ArrayList<Item>>> itemObservers;
    private final ArrayList<IObserver<ArrayList<Invoice>>> invoiceObservers;
    private final ArrayList<IObserver<ArrayList<Staff>>> humanObservers;

    public Model() {
        this.budget = new Budget();
        this.repository = new Repository();
        this.human = new Human();
        this.itemObservers = new ArrayList<>();
        this.invoiceObservers = new ArrayList<>();
        this.humanObservers = new ArrayList<>();
        
        for (Item item: Data.emulateItem()) {
            this.repository.addItem(item);
        }
        
        for (Staff staff: Data.emulateStaff()) {
            this.human.hire(staff);   
        }
    }
    
    public ArrayList<Item> getAllItems() {
        return this.repository.getAllItems();
    }
    
    public void updateItemObserver() {
        for (IObserver observer: this.itemObservers) {
            observer.update(this.repository.getAllItems());
        }
    }
    
    public void updateInvoiceObserver() {
        for (IObserver observer: this.invoiceObservers) {
            observer.update(this.budget.getInvoiceList());
        }
    }
    
    public void updateHumanObserver() {
        for (IObserver observer: this.humanObservers) {
            observer.update(this.human.getAllStaffs());
        }
    }

    public void checkInvoice(Invoice invoice) throws NotEnoughItemInRepoException {
        if (invoice instanceof SaleInvoice) {
            for (SaleItem item: ((SaleInvoice)invoice).getSaleItems()) {
                if (!repository.checkIfEnoughItem(item.getId(), item.getQuantity())) {
                    throw new NotEnoughItemInRepoException(item);
                }
            }
        }
    }
    
    public void addItem(Item item) {
        this.repository.addItem(item);
        
    }

    public boolean importInvoice(ImportInvoice invoice) {
        this.repository.importItems(invoice);
        this.updateItemObserver();
        this.budget.processInvoice(invoice);
        this.updateInvoiceObserver();
        return true;
    }

    public boolean saleInvoice(SaleInvoice invoice) {
        this.repository.exportItems(invoice);
        this.updateItemObserver();
        this.budget.processInvoice(invoice);
        this.updateInvoiceObserver();
        return true;
    }
    
    public boolean addIncurredInvoice(Invoice invoice) {
        this.budget.processInvoice(invoice);
        this.updateInvoiceObserver();
        return true;
    }
    
    public void addItemObserver(IObserver<ArrayList<Item>> observer) {
        this.itemObservers.add(observer);
        observer.update(this.getAllItems());
    }
    
    public void addInvoiceObserver(IObserver<ArrayList<Invoice>> observer) {
        this.invoiceObservers.add(observer);
        observer.update(this.budget.getInvoiceList());
    }
    
    public void addHumanObserver(IObserver<ArrayList<Staff>> observer) {
        this.humanObservers.add(observer);
        observer.update(this.human.getAllStaffs());
    }
    
    public ArrayList<Item> findItemsByQuery(ArrayList<Query> queries) {
        return this.repository.getItemsByQuery(queries);
    }
    
    public Item findItemsById(String id) throws NotExistItemException{
        try {
            return this.repository.find(id);    
        } catch(NotExistItemException e) {
            throw e;
        }   
    }
    
    public void hire(Staff staff) {
        this.human.hire(staff);
        this.updateHumanObserver();
    }
    
    public void fire(Staff staff) {
        this.human.fire(staff);
        this.updateHumanObserver();
    }
    
    public Staff getStaffById(String id) throws NotExistStaffException {
        return this.human.getStaffById(id);
    }
    
    public void pay(SalaryInvoice invoice) {
        this.budget.processInvoice(invoice);
        this.updateInvoiceObserver();
    }
    
    public void removeInvoice(Invoice invoice){
        this.budget.removeInvoice(invoice);
    }
    
    public ArrayList<Invoice> getAllInvoices(){
        return this.budget.getInvoiceList();
    }
}
