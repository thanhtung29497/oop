package Model;

import java.util.ArrayList;
import java.util.Date;

public class Staff extends Entity {
    private String name;
    private Double salary;
    private StaffPosition position;
    private Double totalHours = 0.0;
    private ArrayList<SalaryRecord> salaryRecords = new ArrayList<SalaryRecord>();
    private String sex;
    private Date dateOfBirth;
    
    public Staff(){

    }

    public Staff( String name1, Date dateOfBirth, Double salary1, StaffPosition position1) {
        super();
        this.name = name1;
        this.dateOfBirth = dateOfBirth;
        this.salary = salary1;
        this.position = position1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return this.dateOfBirth;
    }
    
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setAge(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Double getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Double totalHours) {
        this.totalHours = totalHours;
    }
    
    public StaffPosition getPosition() {
        return position;
    }

    public void setPosition(StaffPosition position) {
        this.position = position;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
    
    public String getSex() {
        return sex;
    }
    
    public ArrayList<SalaryRecord> getSalaryRecords() {
        return salaryRecords;
    }

    public void setSalaryRecords(ArrayList<SalaryRecord> salaryRecords) {
        this.salaryRecords = salaryRecords;
    }
    public void printSalaryRecord() {
        for (SalaryRecord salaryRecord : salaryRecords) {
            System.out.println(salaryRecord.getMonth());
            System.out.println(salaryRecord.getSalaryPaid());
        }
    }
}

