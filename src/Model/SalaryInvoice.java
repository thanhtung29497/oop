/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author thanh
 */
public class SalaryInvoice extends Invoice {
    
    private ArrayList<SalaryPayment> payments;
    
    public SalaryInvoice() {
        this.sumCost = 0.0;
        this.type = InvoiceType.Salary;
        this.payments = new ArrayList<>();
    }
    
    public void addSalaryPayment(SalaryPayment payment) {
        this.payments.add(payment);
        this.sumCost -= payment.getAmount();
    }
    
    public void addSalaryPayments(ArrayList<SalaryPayment> payments) {
        for (SalaryPayment payment: payments) {
            this.payments.add(payment);
            this.sumCost -= payment.getAmount();
        }
    }
    
    @Override
    public Double getSumCost() {
        return this.sumCost;
    }
}
