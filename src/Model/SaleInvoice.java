package Model;

import Data.Data;
import java.util.ArrayList;
import java.util.Date;

public class SaleInvoice extends Invoice {
    private ArrayList<SaleItem> saleItems;

    public SaleInvoice(InvoiceType type, Date createdTime, Staff userHasRole, String description) {
        super(type, createdTime, userHasRole, description);
        this.saleItems = new ArrayList<>();
    }

    public SaleInvoice() {
        this.type = InvoiceType.Sale;
        this.saleItems = new ArrayList<>();
    }

    @Override
    public Double getSumCost() {
        return this.sumCost;
    }

    public ArrayList<SaleItem> getSaleItems(){
        return saleItems;
    }
    
    public void addItem(SaleItem item) {
        this.sumCost += item.getCost() * item.getQuantity();
        this.saleItems.add(item);
    }
    
    public void addItems(ArrayList<SaleItem> items) {
        for (SaleItem item : items) {
            this.sumCost += item.getCost() * item.getQuantity();
            this.saleItems.add(item);
        }
    }
}
