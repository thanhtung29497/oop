/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author macos
 */
public class RevenueAndProfit {
    
    private Date date;
    private Double revenue = 0.0;
    private Double profit = 0.0;
    
    public RevenueAndProfit(){
        
    }
    
    public void setRevenue(Double revenue){
        this.revenue = revenue;
    }
    
    public Double getRevenue(){
        return revenue;
    }
    
    public void setProfit(Double profit){
        this.profit = profit;
    }
    
    public Double getProfit(){
        return profit;
    }
    
    public void setDate(Date date){
        this.date = date;
    }
    
    public Date getDate(){
        return date;
    }
}
