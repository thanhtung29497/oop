package Model;

import utils.IdGenerator;

import java.util.Date;

/**
 * Created by thanh on 10/3/2017.
 */
public class Book extends Item {
    private String author;
    private Integer pageNumber;
    private String publisher;
    private Date releaseDate;
    public Book(){
        super();
        this.setId("B"+ IdGenerator.generateId());

    }
    public Book(String id,String name, String author, Integer pageNumber, String publisher, Integer chapter, Double cost, Double purchase,String basicUnit,Date expirationDate) {
        super(name, cost, purchase);

    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

}
