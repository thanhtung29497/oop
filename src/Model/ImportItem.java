/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author thanh
 */
public class ImportItem extends InvoiceItem {

    private Double purchase;
    private Integer quantity;

    public ImportItem(Item item, Double purchase, Integer quantity) {
        super(item, quantity);
        this.purchase = purchase;
    }
    
    public void setPurchase(Double purchase) {
        this.purchase = purchase;
    }

    public Double getPurchase() {
        return purchase;
    }

}

