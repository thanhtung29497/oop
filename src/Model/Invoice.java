package Model;

import java.util.Date;

public abstract class Invoice extends Entity {
    protected InvoiceType type;
    private Date createdTime;
    protected Double sumCost;
    private Staff userHasRole; // người chịu trách nhiệm
    private String description; // ghi chú

    public Invoice() {
        this.createdTime = new Date();
        this.sumCost = 0.0;
        this.description = "";
        this.sumCost = 0.0;
    }

    public Invoice(InvoiceType type, Date createdTime, Staff userHasRole, String description) {
        this.type = type;
        this.createdTime = createdTime;
        this.sumCost = 0.0;
        this.userHasRole = userHasRole;
        this.description = description;
    }

    public Invoice(InvoiceType type, Date createdTime, String description, Double cost) {
        this.type = type;
        this.createdTime = createdTime;
        this.description = description;
        this.sumCost = cost;
    }
    
    public Invoice(InvoiceType type, Date createdTime, Staff userHasRole) {
        this.type = type;
        this.createdTime = createdTime;
        this.description = "";
        this.sumCost = 0.0;
        this.userHasRole = userHasRole;
    }

    public String getDescription() {
        return description;
    }

    public abstract Double getSumCost();

    public Date getCreatedTime() {
        return createdTime;
    }

    public Staff getUserHasRole() {
        return userHasRole;
    }

    public InvoiceType getType() {
        return type;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public void setType(InvoiceType type) {
        this.type = type;
    }
    
}
