/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.util.ArrayList;
import Model.*;
import java.util.Date;

/**
 *
 * @author macos
 */
public class Data {    
    public Data(){
    }
    
    public static ArrayList<Item> emulateItem() {
        Item item1 = new Item();
        item1.setName("De men phieu luu ky");
        item1.setCost(20000.0);
        item1.setPurchase(20000.0);
        item1.setQuantity(24);
        
        Item item2 = new Item();
        item2.setName("Rua va tho");
        item2.setPurchase(10000.0);
        item2.setCost(50000.0);
        item2.setQuantity(10);
        
        ArrayList<Item> items = new ArrayList<>();
        items.add(item1);
        items.add(item2);
        return items;
        
    }
    
    public static ArrayList<Staff> emulateStaff() {
        Staff staff1 = new Staff();
        staff1.setName("Trần Văn A");
        staff1.setDateOfBirth(new Date());
        staff1.setPosition(StaffPosition.Manager);
        staff1.setSalary(1000000.0);
        staff1.setSex("Male");
        
        Staff staff2 = new Staff();
        staff2.setName("Nguyễn Văn B");
        staff2.setDateOfBirth(new Date());
        staff2.setPosition(StaffPosition.Accountant);
        staff2.setSalary(1000000.0);
        staff2.setSex("Female");
        
        ArrayList<Staff> staffs = new ArrayList<>();
        staffs.add(staff1);
        staffs.add(staff2);
        return staffs;
        
//        products.add(item1);
//        products.add(item2);
//        System.out.print(products.size());
    }
    
    
    
    public static ArrayList<Item> products = new ArrayList<Item>();
    public static ArrayList<ImportInvoice> importInvoices = new ArrayList<ImportInvoice>();
    public static ArrayList<SaleInvoice> saleInvoices = new ArrayList<SaleInvoice>();
    public static ArrayList<IncurredCostInvoice> incurredCosts = new ArrayList<IncurredCostInvoice>();
    public static ArrayList<Staff> staffs = new ArrayList<Staff>();
    
    
    
    
}
