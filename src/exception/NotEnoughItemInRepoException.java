package exception;

import Model.SaleItem;

public class NotEnoughItemInRepoException extends Exception {
    private SaleItem item;

    public NotEnoughItemInRepoException(SaleItem item) {
        this.item = item;
    }

    public SaleItem getItem() {
        return item;
    }
}
