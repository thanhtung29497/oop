/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package query;

import Model.Item;

public class QueryID extends Query {

    public QueryID(String value) {
        super(value);
    }
    public boolean isMatched(Item item) {
        if (this.value == "" || this.value == null) {
            return true;
        }
        return item.getId().toLowerCase().contains(this.value.toString().toLowerCase());
    }
}
