/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package query;

import Model.Item;

public abstract class Query<ValueType> {
    protected final  ValueType value;
    
    public Query(ValueType value) {
        this.value = value;
    }
    
    public abstract boolean isMatched(Item item);
}
